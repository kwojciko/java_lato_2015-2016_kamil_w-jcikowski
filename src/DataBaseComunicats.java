import javax.swing.*;
import java.io.IOException;
import java.net.*;

public class DataBaseComunicats extends Thread {

    private DatagramSocket socket;
    private InetAddress serverIp;
    protected Object[][] tab;

    private boolean loginStatus =false;
    private boolean registerStatus =false;

    public DataBaseComunicats() throws IOException {
        try {
            this.socket = new DatagramSocket();

        } catch (SocketException e) {
            e.printStackTrace();
        }


    }

    public void run() {
        while (true) {

            byte[] data = new byte[4096];
            DatagramPacket packet = new DatagramPacket(data, data.length);

            try {
                socket.receive(packet);
                String message = new String(packet.getData());
                message=message.trim();
                if(message.equals("1")) loginStatus =true;
                if(message.equals("11")) registerStatus=true;
                if(message.substring(0,2).equals("71"))
                {
                    message=message.trim();
                    String[] tab1 = message.split(",");
                    tab= new Object[tab1.length][2];
                    for(int i=1;i<tab1.length;i++) tab[i-1]= tab1[i].split("   ");
                }
            }

            catch(IOException e){System.out.print(e);}
        }
    }

    public void send(byte[] data,String adress)
    {
         try {
             if(adress!=null) this.serverIp= InetAddress.getByName(adress);
             else{serverIp = serverIp.getLocalHost();}
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        DatagramPacket packet=new DatagramPacket(data, data.length,serverIp,1331);

        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public boolean isLoginStatus() {
        return loginStatus;
    }

    public boolean isRegisterStatus() {
        return registerStatus;
    }


    public void fillPanel(JTable rankingTable) {

        rankingTable.setModel(new javax.swing.table.DefaultTableModel(
                tab,

                new String [] {
                        "Nazwa gracza", "Ilosc zdobytych pkt"
                }
        ));
        rankingTable.setCellSelectionEnabled(true);
        rankingTable.getTableHeader().setReorderingAllowed(false);
    }
}


