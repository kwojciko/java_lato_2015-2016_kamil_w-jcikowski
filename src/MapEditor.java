import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MapEditor extends JFrame implements ActionListener {

    MapCanvas canvas= new MapCanvas();
    JButton bempty = new JButton();
    JButton bbrick = new JButton();
    JButton bsave = new JButton();
    JButton bexit = new JButton();
    public JTextField textName = new JTextField();
    JButton mapName = new JButton();


    public MapEditor()
    {
        setSize(645,540);
        setLayout(null);
        setResizable(false);
        setTitle("Map Editor");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);

        add(canvas);

        JPanel background= new JPanel();
        Border redline = BorderFactory.createLineBorder(Color.red,2);
        background.setBounds(0,440,639,72);
        background.setBackground(Color.gray);
        background.setBorder(redline);
        background.setVisible(true);
        background.setLayout(null);



        bbrick.setIcon(new ImageIcon("pictures//brick40x40.png"));
        bsave.setIcon(new ImageIcon("pictures//save.png"));
        bexit.setIcon(new ImageIcon("pictures//exit2.png"));

        bbrick.setBounds(10,455,40,40);
        add(bbrick);
        bbrick.setVisible(true);
        bbrick.repaint();
        bbrick.addActionListener(this);

        bempty.setBackground(Color.black);
        bempty.setBounds(60,455,40,40);
        add(bempty);
        bempty.setVisible(true);
        bempty.repaint();
        bempty.addActionListener(this);

        bsave.setBounds(550,442,70,35);
        bsave.setVisible(true);
        add(bsave);
        bsave.addActionListener(this);


        bexit.setBounds(550,475,70,35);
        bexit.setVisible(true);
        add(bexit);
        bexit.addActionListener(this);

        bexit.repaint();
        bsave.repaint();



        textName.setBounds(450,475,100,35);
        add(textName);
        textName.repaint();

        mapName.setBounds(465,450,70,25);
        mapName.setIcon(new ImageIcon("pictures//mapname.png"));
        mapName.setVisible(true);
        mapName.setBorder(null);
        add(mapName);


        add(background);






    }

    public static void main(String[] args) {

        MapEditor mapEditor = new MapEditor();



    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Object sourceEvent =e.getSource();


        if(sourceEvent==bempty)  canvas.chose(0);
        if(sourceEvent==bbrick)  canvas.chose(2);
        if(sourceEvent ==bexit)  dispose();
        if(sourceEvent ==bsave) {

            if(!textName.getText().equals(""))
            {

                canvas.saveMap(textName.getText());



            }



        }

    }
}
