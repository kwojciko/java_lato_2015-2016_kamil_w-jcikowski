import java.net.DatagramSocket;
import java.net.SocketException;

public class ScoreServerSocket extends Thread {

    private Serwer serwer;
    private DatagramSocket socket;
    private PlayerMP temp;


   public ScoreServerSocket(Serwer serwer) {
       this.serwer = serwer;
       try {
           this.socket = new DatagramSocket();
       } catch (SocketException e) {
           e.printStackTrace();
       }
   }

    public void run() {

        while(true){

            for(int i=0; i<serwer.playerConnected.size();i++)
            {
                temp= serwer.playerConnected.get(i);
                serwer.sendScoreToAll(temp);

            }

            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

    }






}
