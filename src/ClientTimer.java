import java.util.Timer;
import java.util.TimerTask;

public class ClientTimer {

    int secondsRemaining =0;
    Timer timer = new Timer();
    TimerTask task = new TimerTask() {

        @Override
        public void run() {
            if(secondsRemaining>0) secondsRemaining--;
        }
    };

    public ClientTimer(int time)
    {
        this.secondsRemaining=time;

    }


    public void start()
    {
        timer.scheduleAtFixedRate(task,1000,1000);


    }
}
