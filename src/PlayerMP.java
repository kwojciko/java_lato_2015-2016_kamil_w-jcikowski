import java.io.IOException;
import java.net.InetAddress;

public class PlayerMP  {

    protected int id;
    protected int moveImageId;
    protected String name;
    protected InetAddress ipadress;
    public int x;
    public int y;
    protected int score=0;
    private int frags;
    private int deaths;

    public PlayerMP(String name, InetAddress ipadress,int id,int moveImageId) throws IOException {


        this.name=name;
        this.ipadress=ipadress;
        this.id=id;
        this.moveImageId=moveImageId;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }


    public void setY(int y) {
        this.y = y;
    }


public void addScore(int points)
{
    score+=points;
}

    public int getScore()
    {return score;}

    public void removeScore(int points) {
        score-=points;
    }

    public void addFrag() {
        frags++;
    }

    public void addDeath() {
        deaths++;
    }

    public int getDeaths() {
        return deaths;
    }

    public int getFrags() {
        return frags;
    }
}
