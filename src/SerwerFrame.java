import javax.swing.*;
import java.awt.*;

/**
 * Created by Acer on 2016-05-22.
 */
public class SerwerFrame extends JFrame {
    private JPanel serwerPanel;
    protected JButton startButton;
    protected JTextField timeField;
    private JLabel timeLabel;
    private SerwerListener listener= new SerwerListener(this);
    protected Serwer serwer;

    public SerwerFrame(Serwer serwer)
    {
        super("Serwer");
        setSize(new Dimension(100,100));
        this.serwer = serwer ;
        startButton.addActionListener(listener);
        setContentPane(serwerPanel);
        setVisible(true);

    }
}
