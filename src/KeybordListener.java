import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

public class KeybordListener implements KeyListener {

    private Game game;
    private ScoreBoard board;

    public KeybordListener(Game game)
    {
        board= new ScoreBoard();
        this.game = game;


    }

    public void keyTyped(KeyEvent e)
    {

    }


    public void keyPressed(KeyEvent e) {

        if(KeyEvent.VK_SPACE!=e.getKeyCode()) {
            try {

                game.move(e);

            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }



    }


    public void keyReleased(KeyEvent e) {
        if(KeyEvent.VK_SPACE ==e.getKeyCode())

            try
            {

                game.move(e);

            }

            catch (IOException e1)
            {
                e1.printStackTrace();
            }



    }
}
