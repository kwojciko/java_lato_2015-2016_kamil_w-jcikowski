import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Client extends Thread {


    protected DatagramSocket socket;
    private File tankUp =new File("pictures//czolg.png");
    private File tankDown =new File("pictures//czolgdol.png");
    private File tankRight =new File("pictures//czolgprawo.png");
    private File tankLeft =new File("pictures//czolglewo.png");
    protected File tankBoom = new File("pictures//tankBoom.gif");
    private BufferedImage image = ImageIO.read(tankUp);
    protected List<PlayerPainter> players= new ArrayList<PlayerPainter>();
    private Iterator<PlayerPainter> iter = players.iterator();
    private PlayerPainter i;
    private Player player;
    private FontMetrics metrics;
    private ClientTimer timer = new ClientTimer(0) ;


    public Client(int port, Player player) throws IOException {
        try {
            this.player=player;
            this.socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public void run() {

        while (true) {
            iter = players.iterator();
            byte[] data = new byte[4096];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String message = new String(packet.getData()).trim();
            String[] tabString = message.split(",");


            switch (message.substring(0, 2))
            {
                case "00":
                    PlayerPainter player ;
                    player = new PlayerPainter(Integer.parseInt(tabString[2]),Integer.parseInt(tabString[3]),Integer.parseInt(tabString[1]),tabString[4].trim());
                    player.moveId=Integer.parseInt(tabString[5]);
                    System.out.print(player.id+" added to array list and his moveid is " + player.moveId);
                    players.add(player);


                break ;

                case "01":
                    while(iter.hasNext()){
                         i=iter.next();
                        if(i.id == Integer.parseInt(tabString[1])) {
                            i.x = Integer.parseInt(tabString[2]);
                            i.y = Integer.parseInt(tabString[3]);
                            i.moveId=Integer.parseInt(tabString[4]);
                            System.out.print(i.id + "  x: " + i.x + "  y: " + i.y + "\n");

                        }
                    }

                    break;

                case "99":
                  /*  while(iter.hasNext()){
                        i=iter.next();
                        if(i.id == Integer.parseInt(tabString[1]))
                            {players.remove(i); System.out.print("usn");
                            // iter = players.iterator();
                            }
                        */
                        for(int i=0;i<players.size();i++)
                        {
                            PlayerPainter x = players.get(i);
                            if(x.id == Integer.parseInt(tabString[1]))
                            {players.remove(i); System.out.print("usn");
                        }

                    }

                    break;


                case "05":
                    String[] tab = message.split(",");
                    int tempx= Integer.parseInt(tab[2]);
                    int tempy= Integer.parseInt(tab[3]);
                    int tempMove= Integer.parseInt(tab[4]);
                    int tempId = Integer.parseInt(tab[1]);
                    Bullet temp = new Bullet(tempx,tempy,tempMove,tempId);
                    System.out.print(tempId);
                    this.player.bullets.add(temp);


                    break;


                case "88":

                        tab=message.split(",");
                        this.player.game.map.instertMap(message);
                        this.player.game.findPlace();
                        if(tab[2].equals("1")) this.player.movePossible=true;
                        else this.player.movePossible=false;



                    break;


                case "89":

                   message = this.player.game.map.creatString(this.player.getX(),this.player.getY());
                    System.out.print("daje Ci mape");
                    this.player.send(("90"+message).getBytes());

                    break;

                case "70":
                    tabString=message.split(",");
                    System.out.print("ID "+tabString[1]+"score:  " +tabString[2]);
                    for(int i=0; i<players.size();i++)
                    {
                        player= players.get(i);
                        if(player.id== Integer.parseInt(tabString[1])) player.score=Integer.parseInt(tabString[2].trim());
                    }

                   if (this.player.id==Integer.parseInt(tabString[1])) this.player.score=Integer.parseInt(tabString[2].trim());
                    break;


                case "55":
                    this.player.gameIsStarted=true;
                    this.player.movePossible=true;
                    this.player.counter=0;
                    timer = new ClientTimer(Integer.parseInt(tabString[1]));
                    timer.start();


                    break;


                case "56" :

                    this.player.gameIsStarted=false;
                    this.player.movePossible=false;
                    this.player.winPanel=true;
                    this.player.lastWinner= tabString[1];

                    break;

            }


        }


    }


    public void paint(Graphics2D g2d) throws IOException {

        PlayerPainter temp;
        int width;

        for(int i=0;i<players.size();i++)
        {
            temp=players.get(i);


                if(temp.moveId==1) image=ImageIO.read(tankUp);
                if(temp.moveId==2) image=ImageIO.read(tankDown);
                if(temp.moveId==3) image=ImageIO.read(tankRight);
                if(temp.moveId==4) image=ImageIO.read(tankLeft);
                if(temp.moveId==0) image=ImageIO.read(tankBoom);
                g2d.drawImage(image,temp.x,temp.y,null);


                g2d.setColor(Color.WHITE);
                g2d.setFont(new Font("TimesRoman", Font.PLAIN, 12));
                metrics = g2d.getFontMetrics();
                width = metrics.stringWidth(temp.name);
                g2d.drawString(temp.name,(temp.getX()+20)-(width/2),temp.getY()+50);
                width = metrics.stringWidth(temp.name+ " " + temp.score);
                g2d.drawString(temp.name +" " +temp.score, 690-(width/2) , 100 + 30*i);


            if(this.player.winPanel)
            {
                this.player.counter++;
                BufferedImage image2 = ImageIO.read(new File("pictures//win.png"));
                g2d.drawImage(image2,0,0,null);
                g2d.setFont(new Font("TimesRoman", Font.PLAIN, 44));
                g2d.drawString(this.player.lastWinner,100,100);
                if(this.player.counter>100) this.player.winPanel =false;
            }

        }

        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 44));
        g2d.drawString(timer.secondsRemaining+"",660,400);
        if(!this.player.gameIsStarted && !this.player.winPanel) g2d.drawString("Gra zaraz się rozpocznie",60,200);
    }
}





