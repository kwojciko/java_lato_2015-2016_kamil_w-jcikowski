
public class PlayerPainter {


    public int id;
    public int moveId = 1;
    public int x;
    public int y;
    public String name;
    public int score;
    public int deaths;
    public int frags;

    public PlayerPainter(int x, int y, int id,String name) {
        this.x = x;
        this.id = id;
        this.y = y;
        this.name=name;
        this.moveId = 1;
        System.out.print(name+"\n");
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
