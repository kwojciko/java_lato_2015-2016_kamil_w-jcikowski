import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Acer on 2016-05-22.
 */
public class SerwerListener implements ActionListener {

    private SerwerFrame frame;



    public SerwerListener(SerwerFrame serwerFrame) {

        this.frame = serwerFrame;

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        frame.serwer.gameIsStarted=true;
        int time = Integer.parseInt(frame.timeField.getText());
        frame.serwer.timer = new MyTimer(frame.serwer,time);
        frame.serwer.sendStart(time);
        frame.serwer.timer.start();
        frame.startButton.setVisible(false);




    }
}
