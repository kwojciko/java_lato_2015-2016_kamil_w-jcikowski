
public class Bullet {

    private int x;
    private int y;
    private int move;
    private int id;




    public Bullet(int x, int y, int move,int id) {

        if(move==1)
        {   this.x=x+15;
            this.y=y-5;
        }

        if(move==2)
        {   this.x=x+15;
            this.y=y+45;
        }

        if(move==3)
        {   this.x=x+45;
            this.y=y+15;
        }

        if(move==4)
        {   this.x=x-5;
            this.y=y+15;
        }





        this.move=move;
        this.id=id;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x= x;
    }

    public void setY(int y) {
        this.y= y;
    }

    public int getMove() {
        return move;
    }

    public int getId() {
        return id;
    }
}
