import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;


public class Game extends JPanel implements Runnable {

    public Map map = new Map();
    public GameWindow window;
    private BufferedImage brick = ImageIO.read(new File("pictures//brick.png"));
    public Player player = new Player(this);
    protected int timer=-1;
    private FontMetrics metrics;



    public void move(KeyEvent e) throws IOException {

        int keycode = e.getKeyCode();
        if (keycode == KeyEvent.VK_D && player.getX() < 600)            //ruch w prawo
        {


           if(player.movePossible) this.player.turnRight();
            if (map.getMapValue((player.getY() / 10), (player.getX() / 10 + 4)) == 0 && map.getMapValue((player.getY() / 10 + 3), (player.getX() / 10) + 4) == 0) {

                player.tankRight();
                map.setPosition(player.getX(), player.getY(), 1);


            }
        }

        if (keycode == KeyEvent.VK_S && player.getY() < 400)        //ruch w dół
        {
            if(player.movePossible) this.player.turnDown();
            if (map.getMapValue((player.getY() / 10) + 4, (player.getX() / 10)) == 0 && map.getMapValue((player.getY() / 10) + 4, (player.getX() / 10) + 3) == 0) {
                player.tankDown();
                map.setPosition(player.getX(), player.getY(), -2);
            }
        }


        if (keycode == KeyEvent.VK_W && player.getY() > 0)      //ruch w gore
        {
            if(player.movePossible) this.player.turnUp();
            if (map.getMapValue((player.getY() / 10 - 1), (player.getX() / 10)) == 0 && map.getMapValue((player.getY() / 10 - 1), (player.getX() / 10 + 3)) == 0) {
                player.tankUp();
                map.setPosition(player.getX(), player.getY(), 2);
            }
        }


        if (keycode == KeyEvent.VK_A && player.getX() > 0) {
            if(player.movePossible) this.player.turnLeft();
            if (map.getMapValue((player.getY() / 10), (player.getX() / 10 - 1)) == 0 && map.getMapValue((player.getY() / 10 + 3), (player.getX() / 10 - 1)) == 0) {
                player.tankLeft();
                map.setPosition(player.getX(), player.getY(), -1);
            }
        }


        if (keycode == KeyEvent.VK_SPACE) {
            player.tankShot();
        }

        if (keycode == KeyEvent.VK_TAB)
            map.print();
    }

    public Game(GameWindow window) throws IOException {

        setBackground(Color.BLACK);
        KeyListener listener = new KeybordListener(this);
        addKeyListener(listener);
        setFocusable(true);
        Thread thread = new Thread(this);
        thread.start();
        this.window= window;

    }

    public void paint(Graphics g) {

        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 24));
        metrics = g2d.getFontMetrics();
        int width = metrics.stringWidth(String.valueOf(player.score));
        g2d.drawString("Score: ",640,20);
        g2d.drawString(String.valueOf(player.score),690-(width/2),50);



        for (int i = 0; i < 44; i++) {
            for (int j = 0; j < 64; j++) {
                if (map.getMapValue(i, j) == 2)
                    g2d.drawImage(brick, (j * 10), (i * 10), null);
            }
        }



        try {
            player.paint(g2d);
            TimeUnit.MILLISECONDS.sleep(33);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (IOException e){}
        repaint();
    }


    public static void main(String[] args) throws InterruptedException, IOException {


        GameWindow frame = new GameWindow();


    }


    public void run() {

        int tempx;
        int tempy;
        Bullet x = null;
        while (true) {

            player.iter = player.bullets.iterator();

            for (int i = 0; i < player.bullets.size(); i++) {
                x = player.bullets.get(i);
                tempx = x.getX();
                tempy = x.getY();

                if (x.getX() < 640 && x.getY() < 440 && x.getX() > 0 && x.getY() > 0) {
                    if (x.getMove() == 1) x.setY(tempy - 10);
                    if (x.getMove() == 2) x.setY(tempy + 10);
                    if (x.getMove() == 3) x.setX(tempx + 10);
                    if (x.getMove() == 4) x.setX(tempx - 10);
                    if (map.checkCollision(tempx, tempy, x.getMove())) {
                        player.remove(x);
                        player.iter = player.bullets.iterator();
                    }

                    if (checkHitMulti(x)) {

                        player.remove(x);
                        player.iter = player.bullets.iterator();


                    }

                    if(checkHitPlayer(x))
                    {
                        if(timer==-1) timer=0;
                        player.movePossible = false;
                        player.remove(x);
                        player.iter = player.bullets.iterator();
                        try {
                            player.image = ImageIO.read(player.tankBoom);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }


                } else {
                    player.remove(x);
                    player.iter = player.bullets.iterator();
                }
            }
            try {
                sleep(33);
                if(timer>-1) timer++;
                if(timer>100) player.respawn();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public int checkMap(int a, int b) {
        return (map.getMapValue(a, b));
    }

    public boolean checkHitPlayer(Bullet x) {


        for (int j = 0; j < 40; j += 10)
            for (int k = 0; k < 40; k += 10) {
                if ((x.getX() - 5) == player.getX() + j && (x.getY() - 5) == player.getY() + k) {

                    if(player.movePossible) player.send(("66,"+x.getId()+","+player.id).getBytes());
                    return true;

                }
            }
        return false;
    }

    public boolean checkHitMulti(Bullet x) {
        for (int z = 0; z < player.client.players.size(); z++) {
            PlayerPainter temp = player.client.players.get(z);
            for (int j = 0; j < 40; j += 10)
                for (int k = 0; k < 40; k += 10) {
                    if ((x.getX() - 5) == temp.getX() + j && (x.getY() - 5) == temp.getY() + k) {
                        temp.moveId= 0 ;
                        return true;
                    }
                }

        }
        return false;
    }



    public void findPlace() {

        Random randomGenerator = new Random();
        player.movePossible =true;
        while(true)
        {
            boolean binded = false;

            int tempx = randomGenerator.nextInt(60);
            int tempy = randomGenerator.nextInt(40);
            System.out.print(tempx+""+tempy);

            for (int i = tempx; i < tempx + 4; i++) {
                for (int j = tempy; j < tempy + 4; j++) {
                    if(checkMap(j,i)!=0) {
                        binded = true;
                        break;
                    }
                }

                if (binded == true) break;
            }

            if (binded== false) {

                player.setX(tempx*10); player.setY(tempy*10);
                map.setPosition(tempx*10,tempy*10,1);
                player.send(("10,"+player.id+","+tempx*10+","+tempy*10+",").getBytes());
                break;

            }
        }
    }

}
