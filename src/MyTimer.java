import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Acer on 2016-05-22.
 */
public class MyTimer {



    Serwer serwer;
    int secondsRemaining =1;
    Timer timer = new Timer();
    TimerTask task = new TimerTask() {
        @Override
        public void run() {
            if(secondsRemaining>0) secondsRemaining--;
            if(secondsRemaining==10) serwer.sendStop();
            if(secondsRemaining==0) stop();
            System.out.print(secondsRemaining+"\n");

        }
    };


    public MyTimer(Serwer serwer,int time)
    {
        this.serwer=serwer;
        this.secondsRemaining=time+10;
    }

    public void start()
    {
        timer.scheduleAtFixedRate(task,1000,1000);


    }

    public void stop()
    {
        timer.cancel();
        serwer.frame.startButton.setVisible(true);
        serwer.gameIsStarted=false;
        serwer.clearScore();
    }


}
