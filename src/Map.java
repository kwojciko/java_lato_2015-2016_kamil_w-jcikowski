public class Map {


    public int[][] map = new int[44][64];



    public void setPosition (int x,int y,int dir)
    {

        for(int i=0;i<4;i++)
            for(int j=0;j<4;j++)                                  //wpisywanie przemieszczenia na mape
                map[(y / 10)+j][(x / 10) + i] = 1;




        for(int i=0;i<4;i++) {
            if(dir==1)   map[(y / 10)+i ][(x / 10)-1] = 0;
            if(dir==-1)   map[(y / 10) + i ][(x / 10) + 4] = 0;
            if(dir==-2)   map[(y / 10) -1 ][(x / 10)+i] = 0;            //usuwanie starej pozycji
            if(dir==2)    map[(y / 10) +4 ][(x / 10)+i] = 0;
        }


    }

    public void print()
    {
        for(int i=0;i<44;i++) {
            for (int j = 0; j < 64; j++) System.out.print(map[i][j]);

            System.out.print("\n");
        }
    }


    public int getMapValue(int a, int b)
    {
        return map[a][b];
    }


    public void writeBullet(int x,int y,int dir)
    {
        x=x/10;
        y=y/10;
        map[y][x]=8;
        if(dir==1){map[y+1][x]=0;}
        if(dir==2){map[y-1][x]=0;}
        if(dir==3){map[y][x-1]=0;}
        if(dir==4){map[y][x+1]=0;}
    }


    public boolean checkCollision(int x, int y, int move) {

        x=x/10;
        y=y/10;

        if(map[y][x]==2)
        {
            if(move==1 || move==2){

                for(int i=x-1;i<x+3 ;i++) {

                    if(i>=0 && i<=64 && map[y][i]==2) map[y][i]=0;}

            }



            if(move==3 || move==4)
            {
                for(int i=y-1;i<y+3;i++) {
                    if(i>=0 && i<=44 && map[i][x]==2) map[i][x]=0;}

            }

            map[y][x]=0;

            return true;
        }
        return false;

    }

    public void instertMap(String message)
    {
        message.trim();
        int c  =3;
        int temp;

            for(int i=0;i<44;i++)
                for(int j=0;j<64;j++) {

                    temp = Character.getNumericValue(message.codePointAt(c));
                    map[i][j]= temp;
                    c++;
                }


        for(int i=0;i<44;i++) {
            for (int j = 0; j < 64; j++) {
                System.out.print(map[i][j]);
            }
            System.out.print("\n");
        }



    }

    public String creatString(int x,int y)
    {
        x=x/10;
        y=y/10;

        String messageMap = "";
        for(int i=0;i<44;i++)
            for(int j=0;j<64;j++)
            {
                if(i>=y && i<=y+3 && j>=x && j<=x+3) messageMap=messageMap+"0";
                else messageMap = messageMap+map[i][j];
            }

        int c=0;
        for(int i=0;i<messageMap.length();i++)
        {
            c++;
            System.out.print(messageMap.charAt(i));
            if(c==64){System.out.print("\n"); c=0;}
        }


        return messageMap;
    }

    public void clearPosition(int x, int y) {

        x=x/10;
        y=y/10;

        for(int i=0; i<4;i++)
            for(int j=0;j<4;j++)
                map[y+i][x+j]=0;
    }
}
