import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GameWindow extends JFrame implements ActionListener,WindowListener {

    public JTextField login = new JTextField();
    public JPasswordField password = new JPasswordField();
    public JTextField textSerwer = new JTextField();
    JLabel nick = new JLabel("nick");
    JLabel passwordLabel = new JLabel("password");
    JLabel serwer = new JLabel("serwer");
    JLabel background = new JLabel();
    JPanel backgroundPanel = new JPanel();
    JButton play = new JButton();
    JButton exit = new JButton();
    JButton options = new JButton();
    JButton play2 = new JButton();
    JButton register = new JButton("register");
    JButton register2 = new JButton("register2");
    JButton rankingButton = new JButton("ranking");
    Game game;
    JScrollPane rankingPanel = new JScrollPane() ;
    JTable rankingTable = new JTable();
    DataBaseComunicats comunicats;

    public GameWindow() {

        addWindowListener(this);
        setSize(740, 465);
        setBackground(Color.BLACK);
        repaint();
        setResizable(false);
        setTitle("Tanks");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        rankingButton.setBounds(27,315,160,50);
        rankingButton.setIcon(new ImageIcon("pictures//ranking.png"));

        play2.setBounds(227, 315, 170, 50);
        play2.setIcon(new ImageIcon("pictures//play.png"));

        register.setBounds(227, 375,170,50);
        register.setIcon(new ImageIcon("pictures//register.png"));
        register2.setBounds(227, 375,170,50);
        register2.setIcon(new ImageIcon("pictures//register.png"));

        background.setIcon(new ImageIcon("pictures//GH.png"));
        backgroundPanel.add(background);
        backgroundPanel.setBackground(Color.BLACK);
        play.setBounds(227, 125, 170, 50);
        play.setIcon(new ImageIcon("pictures//play.png"));

        options.setBounds(227, 175, 170, 50);
        options.setIcon(new ImageIcon("pictures//options.png"));

        exit.setBounds(227, 225, 170, 50);
        exit.setIcon(new ImageIcon("pictures//exit.png"));

        rankingButton.addActionListener(this);
        play.addActionListener(this);
        options.addActionListener(this);
        exit.addActionListener(this);
        play2.addActionListener(this);
        register.addActionListener(this);
        register2.addActionListener(this);

        add(play).setVisible(false);
        add(exit).setVisible(false);
        add(options).setVisible(false);


        setVisible(true);

        menuFrame();
        try{comunicats = new DataBaseComunicats();}

        catch(IOException e){System.out.print(e);}
        comunicats.start();
    }


    public void menuFrame() {

        play.setVisible(true);
        options.setVisible(true);
        exit.setVisible(true);
        add(backgroundPanel);

    }

    public void optionsFrame() {
        play.setVisible(false);
        options.setVisible(false);
        exit.setVisible(false);

    }


    public static void main(String[] args) {

        GameWindow window = new GameWindow();
        window.menuFrame();


    }


    public void actionPerformed(ActionEvent e) {
        Object sourceEvent = e.getSource();


        if (sourceEvent == exit) dispose();


        if (sourceEvent == play) {
            optionsFrame();

            rankingPanel.setVisible(false);
            play2.setVisible(true);
            rankingButton.setVisible(true);
            register.setVisible(true);
            nick.setForeground(Color.WHITE);
            serwer.setForeground(Color.WHITE);
            passwordLabel.setForeground(Color.WHITE);

            nick.setBounds(240, 125, 100, 50);
            serwer.setBounds(240, 225, 100, 50);
            passwordLabel.setBounds(240,175,100,50);
            login.setBounds(300, 135, 100, 30);
            textSerwer.setBounds(300, 235, 100, 30);
            password.setBounds(300,185,100,30);

            play2.setVisible(true);
            register.setVisible(true);
            login.setVisible(true);
            serwer.setVisible(true);
            textSerwer.setVisible(true);
            password.setVisible(true);
            passwordLabel.setVisible(true);
            register2.setVisible(true);
            nick.setVisible(true);

            add(rankingButton);
            add(passwordLabel);
            add(password);
            add(login);
            add(textSerwer);
            add(nick);
            add(play2);
            add(register);
            add(serwer);
            add(register2);
            play2.repaint();
            register.repaint();
            rankingButton.repaint();
            add(backgroundPanel);
        }

        if(sourceEvent == register)
        {
            play2.setVisible(false);
            register.setVisible(false);
            register2.setVisible(true);


        }

        if(sourceEvent== rankingButton)
        {

            play2.setVisible(false);
            register.setVisible(false);
            login.setVisible(false);
            serwer.setVisible(false);
            textSerwer.setVisible(false);
            password.setVisible(false);
            passwordLabel.setVisible(false);
            register2.setVisible(false);
            nick.setVisible(false);
            play.setBounds(27,315,160,50);
            play.setVisible(true);




            add(backgroundPanel);
            comunicats.send("71".getBytes(),textSerwer.getText());
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }


            comunicats.fillPanel(rankingTable);

            rankingPanel=new JScrollPane(rankingTable);
            rankingPanel.setBounds(200,100,300,300);
            rankingPanel.setVisible(true);

            add(rankingPanel);
            rankingPanel.repaint();

            add(backgroundPanel);
        }

        if(sourceEvent == register2)
        {
            String passText = new String(password.getPassword());
            if(passText.length()>8 && login.getText().length()>3 )
            {
                comunicats.send(("34,"+login.getText()+","+passText).getBytes(),textSerwer.getText());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                if(comunicats.isRegisterStatus()) {
                    JOptionPane.showMessageDialog(null, "Rejestracja powiodla sie");
                    register2.setVisible(false);
                    play2.setVisible(true);
                }
                else{JOptionPane.showMessageDialog(null, "użytkownik o podanej nazwie juz istnieje");}
            }

            else JOptionPane.showMessageDialog(null, "Haslo oraz login musi posiadac co najmniej 8 znakow ");

        }

        if (sourceEvent == options) {
            //optionsFrame();
            MapEditor mapeditor = new MapEditor();
        }


        if (sourceEvent == play2) {
            try {

                game = new Game(this);
                String passText = new String(password.getPassword());
                comunicats.send(("33,"+login.getText()+","+passText).getBytes(),textSerwer.getText());

                try {
                    TimeUnit.SECONDS.sleep(1);
                }
                catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                
                if(comunicats.isLoginStatus())
                {
                    play2.setVisible(false);
                    login.setVisible(false);
                    register.setVisible(false);
                    register2.setVisible(false);
                    nick.setVisible(false);
                    serwer.setVisible(false);
                    passwordLabel.setVisible(false);
                    password.setVisible(false);
                    textSerwer.setVisible(false);
                    game.player.setName(login.getText(), textSerwer.getText());
                    backgroundPanel.setVisible(false);
                    add(game);
                }

                else{

                    JOptionPane.showMessageDialog(null, "Podany login lub haslo sa nieprawidłowe");

                }

            } catch (IOException e1) {

                dispose();
            }

        }
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        try{
            game.player.disconect();
        }
        catch (NullPointerException e1) {
            dispose();
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

