import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Serwer extends Thread {

    private DatagramSocket socket;
    private SerwerLogin login = new SerwerLogin();
    public PlayerMP player;
    List<PlayerMP> playerConnected = new ArrayList<PlayerMP>();
    public Iterator<PlayerMP> iter = playerConnected.iterator();
    private MapServer map = new MapServer();
    private ScoreServerSocket scoreSocket = new ScoreServerSocket(this);
    protected MyTimer timer;
    protected SerwerFrame frame;
    protected boolean gameIsStarted = false;

    public Serwer() throws IOException {
        try {
            this.socket = new DatagramSocket(1331);
            System.out.print("Serwer start" + "\n");

        } catch (SocketException e) {
            e.printStackTrace();
        }
        map.loadMap();
        scoreSocket.start();
        frame = new SerwerFrame(this);
    }

    public void run() {
        while (true) {

            byte[] data = new byte[4096];
            DatagramPacket packet = new DatagramPacket(data, data.length);
            iter = playerConnected.iterator();

            try {
                socket.receive(packet);
                String message = new String(packet.getData());
                String temp = message.substring(3, 7);


                switch (message.substring(0, 2)) {
                    case "00":


                        message=message.trim();
                        String[] tab = message.split(",");
                        int tempx;
                        int tempy;
                        player = new PlayerMP(tab[2], packet.getAddress(), Integer.parseInt(message.substring(3, 7)),Integer.parseInt(tab[3]));
                        playerConnected.add(player);
                        sendPlayersToNewPlayer(player.id, player.ipadress);
                        sendNewPlayer(player);

                        sendMapToPlayer(player.id, player.ipadress);


                        break;

                    case "01":
                        tab=message.split(",");
                        while (iter.hasNext()) {
                            player = iter.next();
                            if (Integer.toString(player.id).equals(temp)) {
                                player.x= Integer.parseInt(tab[2]);                                       //ruch w gore
                                player.y= Integer.parseInt(tab[3]);
                                player.moveImageId = Integer.parseInt(tab[4].trim());
                               // System.out.print(player.x + ";" + player.y + "   " + player.id + "\n");
                                sendPositionToAll(player);
                                break;
                            }
                        }
                        break;

                    case "88":
                        iter = playerConnected.iterator();
                        if(iter.hasNext()) {

                            PlayerMP x = iter.next();
                            send(("89").getBytes(), x.ipadress,x.id );

                        }

                        break;


                    case "99":
                        PlayerMP temp1 = null;

                        for(int i =0; i<playerConnected.size();i++) {
                            temp1= playerConnected.get(i);

                            if (Integer.toString(temp1.id).equals(temp)) {

                                sendDisconectToAll(temp1);
                                System.out.print(temp1.name +" "+temp1.id + " disconect" + "\n");
                                playerConnected.remove(temp1);

                        }


                        }
                        iter = playerConnected.iterator();
                        while (iter.hasNext()) {
                            PlayerMP x = iter.next();
                           // System.out.println(x.name + x.ipadress + " " + x.id);
                        }


                        break;

                    case "71":

                        String wynik = login.ranking();
                        send(("71,"+wynik).getBytes(),packet.getAddress(),packet.getPort());
                     break;

                    case "33":
                        message.trim();
                        tab = message.split(",");
                        if (login.login(tab[1].trim(), tab[2].trim())) {
                            send("1".getBytes(), packet.getAddress(), packet.getPort());
                        }

                        break;


                    case "34":
                        message.trim();
                        String[] tab1 = message.split(",");
                        if (login.register(tab1[1].trim(), tab1[2].trim())) {
                            send("11".getBytes(), packet.getAddress(), packet.getPort());
                        }

                        break;

                    case "02":
                        while (iter.hasNext()) {
                            player = iter.next();
                            if (Integer.toString(player.id).equals(temp)) {
                                player.y += 10;                                        //ruch w dol
                                player.moveImageId = 2;
                               // System.out.print(player.x + ";" + player.y + "   " + player.id + "\n");
                                sendPositionToAll(player);
                                break;
                            }
                        }
                        break;

                    case "03":
                        while (iter.hasNext()) {
                            player = iter.next();
                            if (Integer.toString(player.id).equals(temp)) {
                                player.x += 10;                                    //ruch w prawo
                                player.moveImageId = 3;
                               // System.out.print(player.x + ";" + player.y + "   " + player.id + "\n");
                                sendPositionToAll(player);
                                break;
                            }
                        }
                        break;

                    case "04":
                        while (iter.hasNext()) {
                            player = iter.next();
                            if (Integer.toString(player.id).equals(temp)) {
                                player.x -= 10;
                                player.moveImageId = 4;
                               // System.out.print(player.x + ";" + player.y + "   " + player.id + "\n");
                                sendPositionToAll(player);
                                break;
                            }
                        }
                        break;

                    case "05":
                        message=message.trim();
                        sendBulettToAll(message);

                        break;

                    case "10":

                        message=message.trim();
                         tab = message.split(",");
                         temp= tab[1];

                        tempx = Integer.parseInt(tab[2]);
                        tempy = Integer.parseInt(tab[3]);


                        while (iter.hasNext()) {
                            player = iter.next();
                            if (Integer.toString(player.id).equals(temp)) { player.x=tempx; player.setY(tempy);
                            sendPositionToAll(player);}
                            }
                        break;

                    case "90":
                       System.out.print("\nwczytuje mape\n");
                        map.instertMap(message);

                        break;

                    case "66":
                        tab=message.trim().split(",");
                        System.out.print(" \n zestrzelony " + tab[2]+" przez " + tab[1]+"\n" );
                        for(int i=0;i<playerConnected.size();i++)
                        {
                            player = playerConnected.get(i);
                            if(player.id== Integer.parseInt(tab[1])) {player.addScore(100); player.addFrag();}
                            if(player.id== Integer.parseInt(tab[2])) {player.removeScore(50); player.addDeath();}
                        }
                }


            }

            catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMapToPlayer(int id, InetAddress ipadress) {
        int gameStatus=0;

        String message=map.creatString();
        if(gameIsStarted) {gameStatus=1 ;}
        else {gameStatus=0;}
        send(("88,"+message+","+gameStatus).getBytes(), ipadress, id);
    }


    public void send(byte[] data, InetAddress ipadress, int port) {
        DatagramPacket packet = new DatagramPacket(data, data.length, ipadress, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void sendNewPlayer(PlayerMP newPlayer) {
        iter = playerConnected.iterator();
        while (iter.hasNext()) {
            player = iter.next();
            if (player.id != newPlayer.id)
                send(("00" + "," + newPlayer.id + "," + newPlayer.getX() + "," + newPlayer.getY()+","+newPlayer.name+","+newPlayer.moveImageId).getBytes(), player.ipadress, player.id);
        }


    }


    public void sendPlayersToNewPlayer(int id, InetAddress ipadress) {
        iter = playerConnected.iterator();
        while (iter.hasNext()) {

            player = iter.next();
            if (id != player.id)
                send(("00" + "," + player.id + "," + player.getX() + "," + player.getY()+","+player.name+","+player.moveImageId).getBytes(), ipadress, id);

        }


    }

    public void sendPositionToAll(PlayerMP tempPlayer) {
        iter = playerConnected.iterator();
        while (iter.hasNext()) {
            player = iter.next();
            if (player.id != tempPlayer.id) {
                send(("01" + "," + tempPlayer.id + "," + tempPlayer.getX() + "," + tempPlayer.getY() + "," + tempPlayer.moveImageId).getBytes(), player.ipadress, player.id);
            }
        }

    }

    public void sendBulettToAll(String message) {

        String[] tab = message.split(",");
        iter = playerConnected.iterator();
        while(iter.hasNext()) {
            player = iter.next();
            if (player.id != Integer.parseInt(tab[1]) ) send(message.getBytes(), player.ipadress, player.id);
        }
    }




    public void sendDisconectToAll(PlayerMP disconectPlayer)
    {
        iter = playerConnected.iterator();
        while(iter.hasNext()) {
            player =iter.next();
            if(player.id!=disconectPlayer.id)
                send(("99"+","+disconectPlayer.id+","+disconectPlayer.getX()+","+disconectPlayer.getY()).getBytes(),player.ipadress,player.id);
        }


    }

    public void sendScoreToAll(PlayerMP temp)
    {
        iter = playerConnected.iterator();
        while(iter.hasNext()) {
            player =iter.next();
            send(("70,"+temp.id+","+temp.getScore()+","+temp.getDeaths()+","+temp.getFrags()).getBytes(),player.ipadress,player.id);
        }


    }

    public void sendStart(int time)
    {

        PlayerMP temp;
        for(int i =0; i<playerConnected.size();i++) {
            temp= playerConnected.get(i);
            send(("55,"+time).getBytes(),temp.ipadress,temp.id);
        }


    }


    public static void main(String[] args) throws IOException {

        Serwer serwer = new Serwer();
        serwer.start();



    }


    public void sendStop() {

        String winner = findWinner();

        PlayerMP temp;
        for(int i =0; i<playerConnected.size();i++) {
            temp= playerConnected.get(i);
            login.updateScore(temp.name,temp.score);


            send(("56,"+winner).getBytes(),temp.ipadress,temp.id);
        }
    }

    private String findWinner() {

        PlayerMP temp;
        int score=0;
        String name="";

        for(int i =0; i<playerConnected.size();i++) {
            temp = playerConnected.get(i);
            if(temp.getScore()>score) {score=temp.getScore(); name =temp.name;}
        }
    return name+" jego wynik : "+score;
    }

    public void clearScore() {

        PlayerMP temp;
        for(int i =0; i<playerConnected.size();i++) {
            temp = playerConnected.get(i);
            temp.score=0;
        }
    }
}




