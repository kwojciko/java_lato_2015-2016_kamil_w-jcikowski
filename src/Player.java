import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class Player {
    public String name;
    protected int x = -40;
    protected int y = -40;
    protected int id;
    protected BufferedImage image;
    private String message;
    private final File tankUp = new File("pictures//czolg.png");
    private final File tankDown = new File("pictures//czolgdol.png");
    private final File tankRight = new File("pictures//czolgprawo.png");
    private final File tankLeft = new File("pictures//czolglewo.png");
    protected final File tankBoom = new File("pictures//tankBoom.gif");
    protected DatagramSocket socket;
    protected InetAddress serverIp;
    protected InetAddress ipadress;
    protected final Client client;
    public List<Bullet> bullets = new ArrayList<Bullet>();
    protected int move=1;
    public Iterator<Bullet> iter = bullets.iterator();
    public final Game game;
    protected boolean movePossible = false;
    public int score;
    protected boolean gameIsStarted= false;
    protected boolean winPanel=false;
    protected String lastWinner;
    public int counter =0;

    public Player(Game game) throws IOException {

        Random randomGenerator = new Random();
        id = randomGenerator.nextInt(100) + 2000;
        try {
            this.socket = new DatagramSocket();
            this.ipadress = InetAddress.getLocalHost();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        image = ImageIO.read(tankUp);
        client = new Client(id, this);
        client.start();
        this.game = game;


    }


    public void send(byte[] data) {
        DatagramPacket packet = new DatagramPacket(data, data.length, serverIp, 1331);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void tankUp() throws IOException {
        if(movePossible){
        y = y - 10;
        message = "01," + id+","+x+","+y+","+move;
        send(message.getBytes());

    }}

    public void tankDown() throws IOException {
        if (movePossible) {
            y = y + 10;
            message = "01," + id+","+x+","+y+","+move;
            send(message.getBytes());

        }
    }
    public void tankRight() throws IOException {
        if (movePossible) {
            x = x + 10;
            message = ("01," + id+","+x+","+y+","+move);
            send(message.getBytes());

        }
    }

    public void tankLeft() throws IOException {
        if (movePossible) {
            x -= 10;

            message = "01," + id+","+x+","+y+","+move;
            send(message.getBytes());

        }
    }

    public void tankShot() {
        if (movePossible) {
            Bullet temp = new Bullet(x, y, move,id);
            bullets.add(temp);
            message = "05," + id + "," + x + "," + y + "," + temp.getMove();
            send(message.getBytes());

        }
    }

    public void setName(String name, String adress) {
        this.name = name;
        try {
            if (adress != null) this.serverIp = InetAddress.getByName(adress);
            else this.serverIp = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
        }
        message = new String("88");
        send(message.getBytes());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        message = new String("00," + id + "," + name+","+move);
        send(message.getBytes());


    }

    public void paint(Graphics2D g2d) throws IOException {
        g2d.drawImage(image, x, y, null);
        Bullet x = null;
        iter = bullets.iterator();


        for (int i = 0; i < bullets.size(); i++) {
            x = bullets.get(i);
            g2d.setColor(Color.RED);
            g2d.fillOval(x.getX(), x.getY(), 10, 10);
        }



        client.paint(g2d);
    }

    public void disconect() {
        send(("99," + id).getBytes());
        socket.close();

    }

    public void remove(Bullet x) {

        bullets.remove(x);
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }


    public void turnRight() throws IOException {
        image = ImageIO.read(tankRight);
        move = 3;

    }

    public void turnDown() throws IOException {
        image = ImageIO.read(tankDown);
        move = 2;

    }

    public void turnLeft() throws IOException {
        image = ImageIO.read(tankLeft);
        move = 4;

    }

    public void turnUp() throws IOException {
        image = ImageIO.read(tankUp);
        move = 1;

    }


    public void respawn() {

        try {
            this.game.timer=-1;
            image = ImageIO.read(tankUp);
            game.map.clearPosition(x ,y);
            game.findPlace();

            }


        catch (IOException e) {
            e.printStackTrace();
        }

    }





    }



