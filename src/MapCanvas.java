import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Scanner;



public class MapCanvas extends JPanel  {

    private int[][] map = new int[44][64];
    private BufferedImage brick;
    private BufferedImage brick40;
    JFrame saved ;
    int chose=2;
    int xpos;
    int ypos;
    int temp;

    MouseAdapter mouse = new MouseAdapter() {

            @Override
            public void mousePressed (MouseEvent e){

                int x = 0;                                                  //wstawianie do tablicy po kliknieciu
                int y = 0;
                if (e.getX() < 610 && e.getY() < 410) {
                    for (int i = 0; i < 4; i++) {
                        for (int j = 0; j < 4; j++) {
                            x = (e.getX() / 10) + i;
                            y = (e.getY() / 10) + j;

                            map[y][x] = chose;
                        }

                    }
                    repaint();
                }
            }

       public void mouseMoved(MouseEvent e){


                temp=(e.getX()/10);
                xpos=temp*10;                                               //wyswietlanie kwadracika na wysokosci kursora
                temp=(e.getY())/10;
                ypos=temp*10;
                repaint();


        }

    };

    public MapCanvas() {

        setBackground(Color.black);
        setSize(new Dimension(640, 440));
        setVisible(true);


        addMouseListener(mouse);
        addMouseMotionListener(mouse);


        for (int i = 0; i < 44; i++)
            for (int j = 0; j < 64; j++) map[i][j] = 0;


        try {
            brick = ImageIO.read(new File("pictures//brick.png"));
            brick40 = ImageIO.read(new File("pictures//brick40x40.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        for(int i=0;i<44;i++) {
            for (int j = 0; j < 64; j++)
            {
                if (map[i][j] == 2)
                    g2d.drawImage(brick, (j * 10), (i * 10), null);
            }
        }

       if(chose==2) g2d.drawImage(brick40,xpos,ypos,null);
       if(chose==0) g2d.drawRect(xpos,ypos,40,40);
    }

    public void chose(int i) {chose = i;}

    public void saveMap(String name) {

        {
            try {
                PrintWriter zapis = new PrintWriter("map//"+name+".txt");
                for(int i=0;i<44;i++)
                {
                    for (int j = 0; j < 64; j++)
                    {
                        zapis.print(map[i][j]);
                        zapis.print(" ");                                   //zapis mapy do pliku

                    }
                    zapis.println();
                }
                zapis.close();

                File plik = new File("mapnames.txt");
                Scanner scanner= new Scanner(plik);
                String allFile=scanner.nextLine()+"\n";
                while(scanner.hasNext()) allFile+=scanner.nextLine()+"\n";
                allFile+=name;                                               //utworzenie pliku zawierajacego wszystkie nazwy map
                System.out.print(allFile);
                zapis=new PrintWriter("mapnames.txt");
                zapis.write(allFile);
                zapis.close();




                ActionListener savedListner = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        saved.dispose();
                    }
                };




                JOptionPane.showMessageDialog(null, "Save complete");



            }

            catch (IOException e) { }
        }
    }
}